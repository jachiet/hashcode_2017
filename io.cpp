#include "io.h"

void read_input(Input &input)
{
	cin >> input.nVideos >> input.nEndpoints >> input.nRequests >> input.nCaches >> input.cache_capacity;

	for(int iVid = 0; iVid < input.nVideos; iVid++)
		cin >> input.video_size[iVid]; 

	for(int i=0; i<1000; i++)
		for(int j=0; j<1000; j++)
			input.cache_latency[i][j] = 0;

	for(int i=0; i<input.nEndpoints; i++){
		lli k;
		cin >> input.end_latency[i] >> k;
		for(int j=0; j<k; j++){
			lli c;
			cin >> c;
			cin >> input.cache_latency[i][c];
		}

	}

	for(int i=0; i<input.nRequests; i++){
		cin >> input.requests[i].video_id >> input.requests[i].endpoint_id >> input.requests[i].mult;
	}
}

lli get_score(Input &input, Output &output) {
	bool vid_in_cache[MAX_CACHES][MAX_VIDS];

	for(int iCache = 0; iCache < MAX_CACHES; iCache++)
       	for(int iVid = 0; iVid < MAX_VIDS; iVid++)
			vid_in_cache[iCache][iVid] = false;

	for(int iCache = 0; iCache < input.nCaches; iCache++)
		for(int vid: output.cache_servers[iCache])
			vid_in_cache[iCache][vid] = true;

	lli base_score = 0, new_score = 0;
	for(int iReq = 0; iReq < input.nRequests; iReq++) {
		base_score += input.end_latency[input.requests[iReq].endpoint_id]*input.requests[iReq].mult;

		lli lat_min = input.end_latency[input.requests[iReq].endpoint_id];
		for(int iCache = 0; iCache < input.nCaches; iCache++){
			if(vid_in_cache[iCache][input.requests[iReq].video_id] == false)
				continue;
			lli lat = input.cache_latency[iCache][input.requests[iReq].endpoint_id];
			if(lat != 0 && lat < lat_min)
				lat_min = lat;
		}
		new_score += lat_min*input.requests[iReq].mult;
	}

	return base_score - new_score;
}

void write_output(Output &output)
{
	cout << output.nCaches << endl;
	for(int i=0; i<output.nCaches; i++){
		cout << i << " ";
		for(int v: output.cache_servers[i])
			cout << v << " ";
		cout << endl;
	}
}


void write_output2(Output &output)
{
	cout << output.nCaches << endl;
	for(int i=0; i<output.nCaches; i++){
		cout << i << " " << output.cache_servers[i].size() << " ";
		for(int v: output.cache_servers[i])
			cout << v << " ";
		cout << endl;
	}
}

void read_output2(Output &output)
{
	cin >> output.nCaches;
	for(int i=0; i<output.nCaches; i++){
		lli id;
		cin >> id;
		lli n;
		cin >> n;
		for(int j=0; j<n; j++){
			lli tmp;	
			cin >> tmp;
			output.cache_servers[id].insert(tmp);
		}
	}
}
