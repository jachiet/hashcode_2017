#ifndef io_def
#define io_def

#include<iostream>
#include<vector>
#include<algorithm>
#include<set>

using namespace std;

typedef long long int lli;

const lli MAX_VIDS = 10*1000;
const lli MAX_ENDS = 1000;
const lli MAX_REQS = 1000*1000;
const lli MAX_CACHES = 1000;

struct Request
{
	lli video_id;
	lli endpoint_id;
	lli mult;
};

struct Input
{
	lli nVideos, nEndpoints, nRequests, nCaches, cache_capacity;
	lli end_latency[MAX_ENDS];
	lli cache_latency[MAX_CACHES][MAX_ENDS]; // 0 = no connection
	lli video_size[MAX_VIDS];
	struct Request requests[MAX_REQS];
};

struct Output
{
	lli nCaches;
	set<lli> cache_servers[MAX_CACHES];
};

typedef struct Request Request;
typedef struct Input Input;
typedef struct Output Output;

void read_input(Input&);
lli get_score(Input&, Output&);
void write_output(Output&);
void write_output2(Output&);
void read_output2(Output&);

#endif
