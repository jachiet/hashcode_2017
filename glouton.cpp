#include "io.h"
#include <algorithm>

Input in;
Output out;

int cache_occ[1000];
bool vid_cache[10000][1000];

bool req_opti[1000000];

int main()
{
   read_input(in);
   vector<pair<int, int> > cache_sorted [1000];
   for(int i=0; i<in.nEndpoints; i++) {
      for(int j=0; j<in.nCache; j++)
         if(in.graph[i][j] != 0)
            cache_sorted[i].push_back(make_pair(in.graph[i][j], j));
      sort(cache_sorted[i].begin(), cache_sorted[i].end());
   }
   for(int i=0; i<in.nVideos; i++)
      for(int j=0; j<in.nCache; j++)
         vid_cache[i][j] = false;
   
   while(1) { 
      int bestVideo=0, bestCache=0, bestGain=0;
      int bestReq = -1;

      for(int req=0; req<in.nRequests; req++){
         if(req_opti[req])
            continue;
         Request r = in.requests[req];
         int vid_size = in.video_size[r.video_id];
         int c  = -1;
         int lat = -1;
         for(pair<int, int> cc : cache_sorted[r.endpoint_id])
            if(!vid_cache[r.video_id][cc.second] && in.cache_capacity - cache_occ[cc.second] >= vid_size) {
               c = cc.second;
               lat = cc.first;
               break;
            }
         int gain = r.mult*(in.latency[r.endpoint_id] - lat);
         if(gain > bestGain) {
            bestVideo = r.video_id;
            bestCache = c;
            bestReq = req;
         }
      }

      if(bestReq == -1)
         break;

      cache_occ[bestCache] += in.video_size[bestVideo];
      out.cache_servers[bestCache].insert(bestVideo);
      vid_cache[bestVideo][bestCache] = 1;
      req_opti[bestReq] = 1;
   }

   out.nCache = in.nCache;

   cout << get_score(in, out) << "\n";
   write_output(out);
}
