#!/bin/bash

file=$1 ;
(test -e "$file" || (echo "$file inexistant!" ; exit 1)) && (
echo "[${file}] phase A @ $(date)"  ; 
cat $file | ./a > ${file%%in}mid ;
echo "[${file}] phase B @ $(date)" ;
echo -n "score: " ;
cat ${file%%in}mid  | ./b > ${file%%in}out ;
echo "Finish!" ;)
