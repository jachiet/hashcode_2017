#include <cstdio>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <map>
using namespace std;
const int Tm = 1010 ;

typedef  pair<int,int> pii ;
vector<vector<int>> incomp ; // incomp[id] = incompatibilies of sq id
vector<int> def ; // def[id] = limits r1 c1 r2 c2 of sq id
vector<int> sq[Tm][Tm] ; //sq[x,y] = ids of sq over x,y


void add (int x1, int y1, int x2, int y2)
{
  const int id = incomp.size();
  for(int x = x1 ; x < x2 ; x++ )
    for(int y = y1 ; y < y2 ; y++ )
      sq[y][x].push_back(id);
  incomp.push_back(vector<int>());
  def.push_back(y1);
  def.push_back(x1);
  def.push_back(y2-1);
  def.push_back(x2-1);
}

int R, L, C, H ;
char in [Tm][Tm];
int nbT[Tm][Tm];

bool wayToSort(int i, int j)
{
  if(def[i*4]!=def[j*4])
    return def[i*4]<def[j*4];
  if(def[i*4+1]!=def[j*4+1])
    return def[i*4+1]<def[j*4+1];
  return (def[i*4+3]-def[i*4+1])*(def[i*4+2]-def[i*4+0])>(def[j*4+3]-def[j*4+1])*(def[j*4+2]-def[j*4+0]);
}

int main()
{
  scanf("%d %d %d %d\n",&R,&C,&L,&H);
  for(int y = 0 ; y < C ; y++ )
    scanf("%s\n",in[y]);
  for(int y = 0 ; y < R ; y++ )
    for(int x = 0 ; x < C ; x++ )
      {
        nbT[y+1][x+1] = nbT[y+1][x]+nbT[y][x+1]-nbT[y][x] ;
        if(in[y][x]=='T')
          nbT[y+1][x+1]++;
    }
  for(int y1 = 0 ; y1 < R ; y1++ )
    for(int x1 = 0 ; x1 < C ; x1++ )
      for(int y2 = y1+1 ; y2 <= y1+H && y2 <= R ; y2++ )
        {
          for(int x2 = x1+1 ; x2 <= C && (x2-x1)*(y2-y1) <= H ; x2++ )
            {
              const int nb = nbT[y2][x2] + nbT[y1][x1] - nbT[y2][x1] - nbT[y1][x2] ;
              const int pds = (x2-x1)*(y2-y1) ;
              if( L <= nb && L <= pds-nb) 
                add(x1,y1,x2,y2);
            }
        }

  for(int y = 0 ; y < R ; y++ )
    for(int x = 0 ; x < C ; x++ )
      {
        for(auto a : sq[y][x])
          for(auto b : sq[y][x])
            if(a!=b)
              incomp[a].push_back(b);
        //vector<int> t;
        //sq[y][x].swap(t);
      }
  const int is = incomp.size();
  fwrite (&is, sizeof(int), 1, stdout);
  for(size_t id = 0 ; id < incomp.size() ; id++ )
    {
      fwrite (&def[id*4] , sizeof(int), 4, stdout);

      vector<int>::iterator it;
      sort(incomp[id].begin(), incomp[id].end());
      it = unique (incomp[id].begin(), incomp[id].end());
      incomp[id].resize( distance(incomp[id].begin(),it) );
      
      
      const int s = incomp[id].size() ;
      fwrite (&s, sizeof(int), 1, stdout);
      for(const int i : incomp[id])
        fwrite(&i,sizeof(int),1,stdout);
    }
  return 0;
}
