#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

const int Tm = 1000*1000*5 ;

int *incomp[Tm] ;
int nb_incomp[Tm];
int cur_incomp[Tm] ; 
int nb ;
int def[Tm][4];
int pds[Tm];
bool selected[Tm] ;
int score, nb_select ;
int ids[Tm];
// int gain(int i)
// {
//   int res = -1 ;
//   for(int j = 0 ; j< nb_incomp[i]; j++)
//     {
//       if(cur_incomp[j] == 1 )
//     }
// }

void set(const int id, const int way)
{
  selected[id] = (way>0) ;
  for(int j = 0 ; j < nb_incomp[id] ; j++ )
    cur_incomp[incomp[id][j]]+=way;
  score += pds[id]*way;
  nb_select += way ;
}

void remove_first_incomp(const int id,int &oldsel)
{
  int id_oldsel = 0 ;
  while(!selected[incomp[id][id_oldsel]])
    {
      id_oldsel++ ;
    }
  oldsel=incomp[id][id_oldsel];
  set(oldsel,-1);
}

void sature(const int id)
{
  for(int j = 0 ; j < nb_incomp[id] ; j++ )
    if(cur_incomp[incomp[id][j]]==0)
      if(!selected[incomp[id][j]])
        set(incomp[id][j],1);
}

int change(int newsel, int & oldsel)
{
  const int old_score=score;
  set(newsel,1);
  remove_first_incomp(newsel,oldsel);
  sature(oldsel);
  return score-old_score;
}

vector<int> oldsel ;

void optim(int newsel)
{
  const int old_score=score;
  oldsel.resize(cur_incomp[newsel]) ;
  int drop ;
  set(newsel,1);
  for(size_t i = 0 ; i < oldsel.size() ; i++)
      remove_first_incomp(newsel,oldsel[i]);
  for(size_t i = 0 ; i < oldsel.size() ; i++)
    sature(oldsel[i]);
  
  if(old_score > score)
    {
      for(size_t i = 0 ; i < oldsel.size() ; i++)
        while(cur_incomp[oldsel[i]])
          remove_first_incomp(oldsel[i],drop);
      for(size_t i = 0 ; i < oldsel.size() ; i++)
        set(oldsel[i],1);
    }
}

void do_step(int nb_remove)
{
  int i;
  fprintf(stderr,"\nOptim %d [%d]\n",nb_remove,score);
  for(size_t nb_optim = 0 ; nb_optim < nb*10 ; nb_optim++)
    {
      if(nb_optim<nb)
        i=nb_optim;
      else
        i = rand()%nb;
      if(!selected[i] && cur_incomp[i] <= nb_remove)
        optim(i);
    }

}
int main()
{
  fread(&nb,sizeof(int),1,stdin);
  for(int i = 0 ; i<nb; i++)
    {
      fread(def[i],sizeof(int),4,stdin);
      pds[i] = (def[i][2]-def[i][0]+1)*(def[i][3]-def[i][1]+1);
      fread(nb_incomp+i,sizeof(int),1,stdin);
      incomp[i] = new int[nb_incomp[i]] ;
      fread(incomp[i],sizeof(int),nb_incomp[i],stdin);
      ids[i] = i;
    }
  

  for(int step = 0 ; step < 7 ; step++)
    for(int nb_remove = step ; nb_remove<=step ; nb_remove++)
      {
        do_step(nb_remove);
        do_step(1);
      }
  fprintf(stderr,"final %d\n",score);
  printf("%d\n",nb_select);
  for(int i = 0 ; i < nb ; i++)
    if(selected[i])
      printf("%d %d %d %d\n",def[i][0],def[i][1],def[i][2],def[i][3]);
  
  for(int i = 0 ; i < nb ; i++)
    delete incomp[i];
  return 0;
}
