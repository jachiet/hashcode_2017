#include <cstdio>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>


using namespace std;

typedef long long int lli ;

const lli MAX_VIDS = 10*1000 ;
const lli MAX_EP = 1000 ;
const lli MAX_REQS = 1000*1000 ;
const lli MAX_CACHE = 1000 ;
const lli MAX_CAPACITY = 500*1000 ;

lli nbVids, nbEP, nbReqs, nbCaches, cacheCapacity ;

lli vidSize[MAX_VIDS] ;

map<lli,lli> reqId[MAX_EP] ; // req[ep] maps a vid_id to the total weight of vid_id on ep

lli reqW[MAX_REQS];
multiset<lli> reqCur[MAX_REQS] ;
lli latency[MAX_EP] ;
vector<lli> EPCache[MAX_CACHE] ;
vector<lli> gainCache[MAX_CACHE] ;
lli nbCacheEP[MAX_EP] ;
int curReq ;

lli weightCache[MAX_CACHE];

multiset<lli> curGains[MAX_REQS] ;
lli score ;

void add(lli vid, lli cache)
{
  weightCache[cache]+=vidSize[vid];
  for(size_t id = 0 ; id < EPCache[cache].size() ; id++ )
    {
      const lli ep = EPCache[cache][id] ;
      const lli newGain = gainCache[cache][id] ;
      auto itReq = reqId[ep].find(vid) ;
      if( itReq != reqId[ep].end())
        {
          const lli idReq = itReq->second;
          const lli oldGain= *(reqCur[idReq].rbegin()) ;
          score += max(newGain-oldGain,0ll)*reqW[idReq] ;
          reqCur[idReq].insert(newGain);
        }
    }
  
}

void input()
{
  scanf("%lld %lld %lld %lld %lld", &nbVids, &nbEP, &nbReqs, &nbCaches, &cacheCapacity);

  for(lli vid = 0 ; vid < nbVids ; vid++ )
    scanf("%lld",vidSize+vid);

  
  for(lli ep = 0 ; ep < nbEP ; ep++)
    {
      scanf("%lld %lld",latency+ep,nbCacheEP+ep);
      lli id_cache, gain ;
      for(lli cache = 0 ; cache < nbCacheEP[ep] ; cache++ )
        {
          scanf("%lld %lld",&id_cache,&gain);
          gain = latency[ep]-gain;
          EPCache[id_cache].push_back(ep);
          gainCache[id_cache].push_back(gain);
        }
    }

  for(lli req = 0 ; req < nbReqs ; req++)
    {
      lli vid, ep, w; 
      scanf("%lld %lld %lld",&vid,&ep,&w);
      if(reqId[ep].count(vid)== 0)
          reqId[ep][vid]=curReq++;
      reqW[reqId[ep][vid]]+=w;
    }
}

int main(int argc, char** argv)
{
  input();
  return 0;
}
