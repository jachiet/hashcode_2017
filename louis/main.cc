#include <cstdio>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>


using namespace std;

typedef long long int lli ;

const lli MAX_VIDS = 10*1000 ;
const lli MAX_EP = 1000 ;
const lli MAX_REQS = 1000*1000 ;
const lli MAX_CACHE = 1000 ;
const lli MAX_CAPACITY = 500*1000 ;

lli nbVids, nbEP, nbReqs, nbCaches, cacheCapacity ;

lli vidSize[MAX_VIDS] ;

map<lli,lli> reqId[MAX_EP] ; // req[ep] maps a vid_id to the total weight of vid_id on ep

lli reqW[MAX_REQS];
multiset<lli> reqCur[MAX_REQS] ;
lli reqGain[MAX_REQS];
lli latency[MAX_EP] ;
vector<lli> EPCache[MAX_CACHE] ;
vector<lli> gainCache[MAX_CACHE] ;
lli nbCacheEP[MAX_EP] ;
int curReq ;

lli weightCache[MAX_CACHE];

multiset<lli> curGains[MAX_REQS] ;
multiset<lli> cacheContent[MAX_CACHE] ;
lli score ;

lli stockGain[MAX_CACHE][MAX_VIDS] ;
lli cacheVid[MAX_VIDS];
lli cacheVidVal[MAX_VIDS];
lli max_gain = 0 ;

void computeStockGain()
{
  max_gain = 0;
  for(int c = 0 ; c < nbCaches ; c++)
    for(int v = 0 ; v < nbVids ; v++)
      stockGain[c][v]=0;

  for(int c = 0 ; c < nbCaches ; c++)
    for(size_t id = 0 ; id < EPCache[c].size() ; id++ )
      {
        const lli ep = EPCache[c][id] ;
        const lli newGain = gainCache[c][id] ;
        for(auto it : reqId[ep])
          if(weightCache[c]+vidSize[it.first] <= cacheCapacity)
            stockGain[c][it.first] += reqW[it.second]*max(newGain-reqGain[it.second],0ll);
      }

  for(int c = 0 ; c < nbCaches ; c++)
    for(int v = 0 ; v < nbVids ; v++)
      stockGain[c][v] = (stockGain[c][v]*32768)/vidSize[v] ;

  for(int v = 0 ; v < nbVids ; v++)
    {
      cacheVidVal[v] = -1;
      cacheVid[v] = -1 ;
      for(int c = 0 ; c < nbCaches ; c++)
        {
          if(stockGain[c][v]>max_gain)
            max_gain = stockGain[c][v];
          if(stockGain[c][v]>cacheVidVal[v])
            {
              cacheVid[v] = c;
              cacheVidVal[v] = stockGain[c][v];
            }
        }
    }
}

void prune(lli cache)
{
  vector<lli> useless  ;
  for(lli vid : cacheContent[cache])
    {
      bool useful = false ;
      for(size_t id = 0 ; id < EPCache[cache].size() && !useful ; id++ )
        {
          const lli ep = EPCache[cache][id] ;
          const lli newGain = gainCache[cache][id] ;
          auto itReq = reqId[ep].find(vid) ;
          if( itReq != reqId[ep].end())
            {
              const lli idReq = itReq->second;
              const lli oldGain= *(reqCur[idReq].rbegin()) ;
              useful = reqGain[idReq] == newGain && reqCur[idReq].count(newGain)==1 ;
            }
        }
      if(!useful)
        {
          weightCache[cache]-=vidSize[vid];
          useless.push_back(vid);
          for(size_t id = 0 ; id < EPCache[cache].size() && !useful ; id++ )
            {
              const lli ep = EPCache[cache][id] ;
              const lli newGain = gainCache[cache][id] ;
              auto itReq = reqId[ep].find(vid) ;
              if( itReq != reqId[ep].end())
                {
                  const lli idReq = itReq->second;
                  const lli oldGain= *(reqCur[idReq].rbegin()) ;
                  reqCur[idReq].erase(reqCur[idReq].find(newGain)) ;
                }
            }
        }
    }
  for(auto i : useless)
    {
      cacheContent[cache].erase(cacheContent[cache].find(i)) ;
      fprintf(stderr,"prune %d from %d\n",i,cache);
    }
  
}

lli gain(lli vid, lli cache)
{
  lli res = 0 ;
  for(size_t id = 0 ; id < EPCache[cache].size() ; id++ )
    {
      const lli ep = EPCache[cache][id] ;
      const lli newGain = gainCache[cache][id] ;
      auto itReq = reqId[ep].find(vid) ;
      if( itReq != reqId[ep].end())
        {
          const lli idReq = itReq->second;
          const lli oldGain= *(reqCur[idReq].rbegin()) ;
          reqGain[idReq] = max(newGain,oldGain);
          res += (reqGain[idReq]-oldGain)*reqW[idReq] ;
        }
    }
  return res;
}

void add(lli vid, lli cache)
{
  weightCache[cache]+=vidSize[vid];
  cacheContent[cache].insert(vid);
  for(size_t id = 0 ; id < EPCache[cache].size() ; id++ )
    {
      const lli ep = EPCache[cache][id] ;
      const lli newGain = gainCache[cache][id] ;
      auto itReq = reqId[ep].find(vid) ;
      if( itReq != reqId[ep].end())
        {
          const lli idReq = itReq->second;
          const lli oldGain= reqGain[idReq] ;
          score += max(newGain-oldGain,0ll)*reqW[idReq] ;
          reqCur[idReq].insert(newGain);
          reqGain[idReq] = max(reqGain[idReq],newGain);
        }
    }
  
}

void input()
{
  scanf("%lld %lld %lld %lld %lld", &nbVids, &nbEP, &nbReqs, &nbCaches, &cacheCapacity);

  for(lli vid = 0 ; vid < nbVids ; vid++ )
    scanf("%lld",vidSize+vid);

  
  for(lli ep = 0 ; ep < nbEP ; ep++)
    {
      scanf("%lld %lld",latency+ep,nbCacheEP+ep);
      lli id_cache, gain ;
      for(lli cache = 0 ; cache < nbCacheEP[ep] ; cache++ )
        {
          scanf("%lld %lld",&id_cache,&gain);
          gain = latency[ep]-gain;
          EPCache[id_cache].push_back(ep);
          gainCache[id_cache].push_back(gain);
        }
    }

  for(lli req = 0 ; req < nbReqs ; req++)
    {
      reqCur[req].insert(0);
      lli vid, ep, w; 
      scanf("%lld %lld %lld",&vid,&ep,&w);
      if(reqId[ep].count(vid)== 0)
          reqId[ep][vid]=curReq++;
      reqW[reqId[ep][vid]]+=w;
    }
}

void output()
{
  printf("%lld\n",nbCaches);
  for(int i = 0 ; i < nbCaches ; i++)
    {
      printf("%d",i);
      unordered_set<lli> seen ;
      for(auto c : cacheContent[i])
        if( seen.count(c) == 0)
          {
            seen.insert(c);
            printf(" %lld",c);
          }
      printf("\n");
    }
}

void output2()
{
  printf("%lld\n",nbCaches);
  for(int i = 0 ; i < nbCaches ; i++)
    {
	    printf("%d ", i);
      unordered_set<lli> seen ;
      for(auto c : cacheContent[i])
        if( seen.count(c) == 0)
          {
            seen.insert(c);
          }
      printf(" %d", seen.size());
      for (auto v: seen)
        printf(" %lld", v);
      printf("\n");
    }
}
int main(int argc, char** argv)
{
  input();


  // for(int v = 0 ; v < nbVids ; v++)
  //   for(int c = 0 ; c < nbCaches ; c++ )
  //     if(weightCache[c]+vidSize[v] <= cacheCapacity)
  //       max_gain=max(max_gain,gain(v,c)) ;

  int step = 0 ;
  int nb_add =0;
  bool lastcall = false ;
  do
    {
      computeStockGain();
     fprintf(stderr,"max gain %lld | score %lld | ajouts %d\n",max_gain,score,nb_add);
      nb_add=0;
      max_gain = (max_gain*3) /4;
      for(int v = 0 ; v < nbVids ; v++)
        if(cacheVidVal[v]>=max_gain)
          {
            lli c = cacheVid[v] ;
            //fprintf(stderr,"%d %d %d\n",cacheVid[v],weightCache[c],cacheCapacity);
            if(stockGain[c][v] > max_gain)
              if(weightCache[c]+vidSize[v]<=cacheCapacity)
                add(v,c),nb_add++;
          }
      if( step++ > 10 && (step%10==0) )
        for(int c = 0 ; c < nbCaches ; c++ )
          prune(c);
      if(max_gain==0 && !lastcall)
        {
          for(int c = 0 ; c < nbCaches ; c++ )
            prune(c);
          lastcall= true;
          max_gain = 1;
        }
    }
  while(max_gain>0);
  fprintf(stderr,"Score : %lld\n",score);
  output();
  return 0;
}
