#include "io.h"

Input in;
Output init_sol;

bool in_cache(Output sol, lli vid, lli cache) {
	for (int iVid : sol.cache_servers[cache])
		if (iVid == vid)
			return true;
	return false;
}

lli size_vids(Output sol, lli cache) {
	lli total = 0;
	for (int iVid : sol.cache_servers[cache])
		total += in.video_size[iVid];
	return total;	
}

int main(void) {
	read_input(in);	
	read_output2(init_sol);
	Output new_sol = init_sol;
	lli cur_score = get_score(in, init_sol);

	
	for (int iCache1 = 0; iCache1 < in.nCaches; iCache1++) {
		for (int iVid1 : new_sol.cache_servers[iCache1]) {
			for (int iCache2 = iCache1+1; iCache2 < in.nCaches; iCache2++) {
				for (int iVid2 : new_sol.cache_servers[iCache2]) {

					printf("%d %d %d %d\n", iCache1, iVid1, iCache2, iVid2);

					lli free1 = in.cache_capacity - size_vids(new_sol, iCache1);
					lli free2 = in.cache_capacity - size_vids(new_sol, iCache2);

					if (free1 + in.video_size[iVid1] - in.video_size[iVid2] >= 0 &&
						free2 + in.video_size[iVid2] - in.video_size[iVid1] >= 0) {
						
						Output test_sol = new_sol;
						test_sol.cache_servers[iCache1].erase(iVid1);
						test_sol.cache_servers[iCache1].insert(iVid2);
						test_sol.cache_servers[iCache2].erase(iVid2);
						test_sol.cache_servers[iCache2].insert(iVid1);
						
						lli new_score = get_score(in, test_sol);
						if (new_score > cur_score) {
							cur_score = new_score;
							write_output2(test_sol);
							return 0;
						}
					}
				}
			}
		}
	}
	return 0;
}
